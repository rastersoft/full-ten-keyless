/* Copyright 2022 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef _COMMON_H_
#define _COMMON_H_

#include <stdint.h>
#include <stddef.h>

// 30 ms for antibounce
#define BUTTON_ANTIBOUNCE_VALUE 30

// maximum value for neopixels
#define USB_LED_INTENSITY 16

#define KEYBOARD_NUM_ROWS 12
#define KEYBOARD_NUM_COLUMNS 8

#define MACROS_KEYPRESS_DELAY 1
#define MACROS_KEYPRESS_MODIFIER_DELAY 5

extern const uint16_t kbdScancodes[KEYBOARD_NUM_ROWS * KEYBOARD_NUM_COLUMNS];

#endif // _COMMON_H_
