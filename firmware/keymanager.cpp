/* Copyright 2022 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include <Arduino.h>
#include <avr/power.h>

#include "keymanager.hh"
#include "common.hh"
#include "eventloop.hh"
#include "macros.hh"

static const uint8_t ROWS_LIST[KEYBOARD_NUM_ROWS] = {
                    PIN_D6, PIN_D4,
                    PIN_B6, PIN_D5,
                    PIN_B4, PIN_D3,
                    PIN_D7, PIN_D1,
                    PIN_C6, PIN_D2,
                    PIN_C7, PIN_D0
                  };

static const uint8_t COLUMNS_LIST[KEYBOARD_NUM_COLUMNS] = {
                        PIN_F4, PIN_F1, PIN_F0, PIN_F5,
                        PIN_F6, PIN_F7, PIN_B7, PIN_B5
                     };

typedef struct {
    KeyButton buttons[KEYBOARD_NUM_ROWS * KEYBOARD_NUM_COLUMNS];
    uint8_t old_keyboard_leds;
} Keymanager;

static Keymanager keymanager;
uint8_t fnPressed = 0;

static void setLeds() {
    uint8_t r, g, b;

    r = 0;
    g = 0;
    b = USB_LED_INTENSITY; // turned on
    if (keyboard_leds & USB_LED_CAPS_LOCK) {
        b = 0;
        g = USB_LED_INTENSITY;
    }
    if (keyboard_leds & USB_LED_SCROLL_LOCK) {
        b = 0;
        r = USB_LED_INTENSITY;
    }
    if (fnPressed) {
        b = 0;
        g = USB_LED_INTENSITY;
        r = USB_LED_INTENSITY;
    }
    SK6812NeoPixelSetColor(r, g, b);
    keymanager.old_keyboard_leds = keyboard_leds;
}

static ASYNC_FUNC(keyboardLoop) {
    static KeyButton *button;
    static uint8_t status, i, j;
    static uint16_t scancode;
    static MACRO *macro;
    static bool macro_found;

    BEGIN_ASYNC();
    setLeds();
    while(1) {
        button = keymanager.buttons;
        for (i=0; i < KEYBOARD_NUM_ROWS; i++) {
            digitalWrite(ROWS_LIST[i], LOW);
            for (j=0; j < KEYBOARD_NUM_COLUMNS; j++) {
                status = KeyButtonReadStatus(button);
                if (status & BUTTON_STATUS_CHANGED) {
                    scancode = kbdScancodes[button->scancode];
                    if (scancode == 0xFFFF) {
                        fnPressed = status & BUTTON_STATUS_PRESSED;
                        keymanager.old_keyboard_leds = 0xFF;
                    } else {
                        if (fnPressed) {
                            // process macros
                            if (!(status & BUTTON_STATUS_PRESSED)) {
                                macro_found = false;
                                for (macro = macro_list; macro->keycode != 0xFFFF; macro++) {
                                    if (macro->keycode == scancode) {
                                        macro_found = true;
                                        macro_buffer = (uint8_t *) macro->string;
                                        YIELD(send_macro_buffer);
                                        break;
                                    }
                                }
                                if (!macro_found) {
                                    // If the key pressed hasn't a macro assigned,
                                    // send the key with RIGHT-WINDOW key as modifier
                                    // to allow to generate software-defined macros
                                    Keyboard.press(MODIFIERKEY_GUI);
                                    Keyboard.press(MODIFIERKEY_SHIFT);
                                    Keyboard.press(MODIFIERKEY_CTRL);
                                    Keyboard.press(MODIFIERKEY_ALT);
                                    DELAY(MACROS_KEYPRESS_MODIFIER_DELAY);
                                    Keyboard.press(scancode);
                                    DELAY(MACROS_KEYPRESS_DELAY);
                                    Keyboard.release(scancode);
                                    DELAY(MACROS_KEYPRESS_DELAY);
                                    Keyboard.releaseAll();
                                    DELAY(MACROS_KEYPRESS_MODIFIER_DELAY);
                                }
                            }
                            scancode = 0x00;
                        }
                        if (scancode) {
                            if (status & BUTTON_STATUS_PRESSED) {
                                Keyboard.press(scancode);
                            } else {
                                Keyboard.release(scancode);
                            }
                        }
                    }
                }
                button++;
            }
            digitalWrite(ROWS_LIST[i], HIGH);
        }
        if (keymanager.old_keyboard_leds != keyboard_leds) {
            setLeds();
        }
        PERIODIC_DELAY(1);
    }
    END_ASYNC();
}

void KeymanagerInit() {
    uint8_t counter = 0;
    uint8_t i, j;

    Keyboard.begin();
    KeyButton *button = keymanager.buttons;
    for (i=0; i < KEYBOARD_NUM_ROWS; i++) {
        pinMode(ROWS_LIST[i], OUTPUT);
        digitalWrite(ROWS_LIST[i], HIGH);
        for (j=0; j < KEYBOARD_NUM_COLUMNS; j++) {
            // the semirows are "mirrored", so the right half is in the
            // opposite order than the left half
            if (i%2) {
                KeyButtonInit(button, COLUMNS_LIST[KEYBOARD_NUM_COLUMNS - j - 1], counter);
            } else {
                KeyButtonInit(button, COLUMNS_LIST[j], counter);
            }
            counter++;
            button++;
        }
    }
    for (int i=0; i < KEYBOARD_NUM_COLUMNS; i++) {
        pinMode(COLUMNS_LIST[i], INPUT_PULLUP);
    }
    // launch the keyboard thread
    keyboardLoop(NULL);
}
