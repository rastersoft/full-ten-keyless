/* Copyright 2022 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "keybutton.hh"
#include <Arduino.h>

void KeyButtonInit(KeyButton *self, uint8_t pin, uint8_t scancode) {
    self->pin = pin;
    self->scancode = scancode;
    self->status = BUTTON_STATUS_RELEASED;
    self->counter = 0;
}

uint8_t KeyButtonReadStatus(KeyButton *self) {
    if (self->counter) {
        self->counter--;
        return self->status;
    }
    int value = digitalRead(self->pin) ? BUTTON_STATUS_RELEASED : BUTTON_STATUS_PRESSED;
    if (self->status == value) {
        return self->status;
    }
    self->status = value;
    self->counter = BUTTON_ANTIBOUNCE_VALUE;
    return (self->status | BUTTON_STATUS_CHANGED);
}
