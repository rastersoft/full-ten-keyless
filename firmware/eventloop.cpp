/* Copyright 2022 Raster Software Vigo
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "Arduino.h"
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/pgmspace.h>
#include "eventloop.hh"

static EVENT_QUEUE(mainQueue);
static EVENT_QUEUE(timerQueue);
volatile uint64_t msecs;
uint64_t cmpmsecs;
volatile uint8_t extIntEvent;

VAR_WAITER(waiterSpiInt);

ISR(TIMER0_COMPA_vect) {
#if 1
    static uint64_t *imsecs = (uint64_t *)&msecs;
    (*imsecs)++;
    // Since msecs is volatile, it will be stored first and re-read again here;
    // this trick should avoid it, saving some bytes and instructions.
    // But check in your architecture to see which one is smaller
    if ((*imsecs) >= cmpmsecs)
        extIntEvent |= TIMER0_EXT_INT;
#else
    msecs++;
    if (msecs >= cmpmsecs)
        extIntEvent = TIMER0_EXT_INT;
#endif
}

void QueueInsertEvent(EventQueue *queue, Event *event) {
    if (!event) {
        return;
    }
    event->next = NULL;
    if (!queue->head) {
        queue->head = queue->tail = event;
        return;
    }
    queue->tail->next = event;
    queue->tail = event;
}

void QueueInsertEventOrdered(EventQueue *queue, Event *event) {
    if (!event) {
        return;
    }
    if (!queue->head) {
        event->next = NULL;
        queue->head = queue->tail = event;
        return;
    }
    if (queue->head->delay > event->delay) {
        event->next = queue->head;
        queue->head = event;
        return;
    }
    for(Event *pevent = queue->head; pevent->next; pevent = pevent->next) {
        if (pevent->next->delay > event->delay) {
            event->next = pevent->next;
            pevent->next = event;
            return;
        }
    }
    queue->tail->next = event;
    queue->tail = event;
    event->next = NULL;
}

Event *QueueRemoveEvent(EventQueue *queue) {
    Event *retval;

    if (!queue->head) {
        return NULL;
    }
    retval = queue->head;
    queue->head = queue->head->next;
    return retval;
}

void EventLoopInit(void) {
    msecs = 0;
    cmpmsecs = 0xFFFFFFFFFFFFFFFF;
    extIntEvent = 0;
    TIMSK0 |= (1<<OCIE0A); // enable TIMER0 COMPAREA interrupt
}

void EventLoopAddDelay(Event *event, int timeout) {
    if (event->delay != 0) {
        event->delay += timeout;
    } else {
        cli();
        event->delay = msecs+timeout;
        sei();
    }
    QueueInsertEventOrdered(&timerQueue, event);
    cli();
    if (timerQueue.head->delay < cmpmsecs) {
        cmpmsecs = timerQueue.head->delay;
    }
    sei();
}

void EventLoopInsertEvent(Event *event) {
    QueueInsertEvent(&mainQueue, event);
}

void EventLoopRun(void) {
    Event *event;
    uint8_t intEvents;

    while(1) {
        event = QueueRemoveEvent(&mainQueue);
        if ((event != NULL) && (event->callback != NULL)) {
            event->callback(event);
        }
        cli();
        intEvents = extIntEvent;
        extIntEvent = 0;
        if ((intEvents == 0) && (event == NULL)) {
            SMCR = 0x01;
            asm volatile (
                "sei\n\t"
                "sleep\n\t"
            );
            SMCR = 0x00;
        }
        sei();
        if (intEvents & TIMER0_EXT_INT) {
            while(1) {
                event = QueuePeekEvent(&timerQueue);
                cli();
                if (!event || (event->delay > msecs)) {
                    if (event) {
                        cmpmsecs = event->delay;
                    } else {
                        cmpmsecs = 0xFFFFFFFFFFFFFFFF;
                    }
                    sei();
                    break;
                }
                sei();
                EventLoopInsertEvent(QueueRemoveEvent(&timerQueue));
            }
        }
        if (intEvents & SPI_EXT_INT) {
            VarWaiterNotify(&waiterSpiInt);
        }
    }
}

void VarWaiterWait(VarWaiter *waiter, Event *event) {
    if (waiter->changed) {
        waiter->changed = false;
        EventLoopInsertEvent(event);
    } else {
        waiter->waiter = event;
    }
}

void VarWaiterNotify(VarWaiter *waiter) {
    if (waiter->waiter) {
        EventLoopInsertEvent(waiter->waiter);
        waiter->changed = false;
        waiter->waiter = NULL;
    } else {
        waiter->changed = true;
    }
}
