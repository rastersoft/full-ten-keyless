# Printing and building the case

The *keyboard case.FCStd* file contains the CAD design of a case for this
keyboard. Although ideally you should print it in a 3D printer with a bed of
13x33 centimeters, that kind of printer is not very common, so it is possible
to print it in two pieces that can be glued.

![3D case design](../3dcase_design.png)*FreeCAD with all the pieces for the case*

The *left_half* and *right_half* pieces comprise the body itself, and must be
printed separately if your printer has a *normal* bed size. But if you are
fortunate enough to have a big 3D printer, you can export both in the same
STL file and print them in a single operation.

![Printing the left half](printing_left_half.jpg)*Printing the left half piece in a BQ Ephestos 2*

All the other pieces can be printed in a single operation, just grouping all of
them in the bed.

All the pieces are already oriented in the best position to be printed, and
none of them require supports. Also, due to the size of the pieces, I recommend
you to use BRIM as plate adhesion. You don't need raft (not even with all the
other pieces, as long as you print all of them in a single operation).

Due to the low vertical resolution needed, you can print all of them with a
"FAST" profile with 0.3 millimeters layers. It will be OK.

Finally, note that the two halves of the body are designed to use as little
filament as possible. To do so, the majority of the surface is only 0.6
millimeters thick. That's another reason to avoid using *raft*.

![Printing the extra pieces](extra_pieces.jpg)*All the extra pieces can be printed at once, also using BRIM.*

Once printed all the pieces, it's time to mount them. To glue the two halves I
recommend to first put a piece of adhesive tape in one of them:

![Put adhesive tape](adhesive_tape.jpg)*Placing adhesive tape to help gluing both halves.*

Then, put the left half covering half of the tape, align the right half, and put
it too on the tape, ensuring that both are correctly aligned.

![Align both halves](align_halves.jpg)*Putting both halves in the tape and ensuring correct alignment.*

Now you can lift the join point with the tape, apply glue (cyanoacrilate in my case),
and leave it hardening.

![Applying glue](gluing.jpg)*Applying glue in the union. The tape keeps the alignment of both halves.*

It is a good idea to apply some extra glue in the joint to reinforce it.

![Applying extra glue](gluing2.jpg)*Applying extra glue in the joint to reinforce it.*

When the glue has cured, remove the tape.

![Removing the tape](removing_tape.jpg)*Removing the tape.*

Now, separate the extra pieces.

![Separating extra pieces](extra_pieces2.jpg)*The extra pieces, separated and ready to be glued.*

And glue them in the right location. You can see in the top picture with the CAD
design the approximate location of each; the precise location and orientation is
shown in the following pictures:

![Front nuts](front_nuts.jpg)*Placement of the front-left and front-right nuts.
They must be glued to the keys, and the trapezoidal part must be inserted between
them.*

![Diffusor](difusor.jpg)

To create a diffusor for the LEDs, I used translucent
hot glue injected inside the holes of the three LED covers. Just put the piece
with top-down and inject the glue in the hole until it is filled. Wait until it
gets cold, and adjust with a cutter. Don't fill the RESET hole.

![Back nuts and covers](back_nuts.jpg)*Placement of the back nuts and covers.*

Finally, glue the three legs.

![Legs](legs.jpg)*Placement of the three legs.*

Now you can put the keyboard PCB inside the case, and use 1.5 millimeter diameter
screws to fix it to the case (althoug in my case I don't need them because it
is tight).

![Completed keyboard](completed.jpg)*The completed keyboard.*
