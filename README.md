# MiniTKL

A compact ten-keyless keyboard design with ISO and ANSI layouts, 1KHz polling rate,
and three NeoPixeles.

## The idea

I've seen a lot of ten-keyless keyboards which just removes the keypad but keep margins
and a lot of empty space around the keys. I also have seen a lot of compact keyboards, but
nearly all of them forces to use Fn+Key to gain access to some keys that I use regularly.
Also, the usual keys like *Home*, *End*... are in not-really-standard positions.

This is why I designed my own keyboard layout: it has ALL the keys that a ten-keyless
keyboard has, but arranged in a much more compact way, although still preserves a
logical disposition. I wanted this because I'm tired of not being able to have the
keyboard "in front of me", but to the left, to leave space for the mouse. But also,
as said, I want to have ALL the keys. Even the function keys (honestly, I don't
understand why to remove them in compact keyboards).

![My keyboard layout (ISO version)](layout.png)  
*This is the ISO layout. There is also an ANSI layout available.*

Although the layout in the picture is ISO, there are three board designs, each one in
a different branch:

* main: it contains a dual layout, a board that, in theory, supports both layouts. It is
the "working PCB" where any change is done, but I don't recommend to use it, because some
holes overlap and can result in trouble.

* ISO_layout: it is a branch derived from main, but only supports the ISO layout.

* US_layout: it is a branch derived from main, but only supports the US layout.

![My keyboard layout (ANSI version)](layout_ansi.png)  
*ANSI layout.*

Any change in the board is made in *main*, keeping both layouts, and only then it is
merged in each branch, where the discordant elements are removed and the traces are
adjusted.

In version 1.8 an important change has been made in the layouts: now both allows for
both "short" and "long" Left Shift Key. This is because some keycap sets, although
coming with an "ANSI Return key", lack the "short left shift key" and the "< >" key
placed between it and the "Z" key, putting the "<" and ">" symbols in the "," and
"." keys, and accessible through AltGr. Also, some latin users prefer the "ANSI Return
key", but are used to the "short left shift key", so this change allows to mount any
combination.

Thus, when mounting a "short left shift key" plus "< >" key (like in ISO keyboards),
you must mount the K60 and K61 buttons and the D61 diode, and leaving out the KB60
button pads.

Instead, if you want a "long left shift key" without "< >" key (like in ANSI
keyboards), you must mount the KB60 button and leave empty the K60, K61 and D61
pads.

To simplify even further the mounting process, K60, K61 and D61 have an extra
"ISO" text, and KB60 has an extra "ANSI" text.

![My keyboard layout (Hybrid ANSI/ISO version)](layout_ansi_iso.png)  
*Hybrid ANSI/ISO layout.*

## The design

I used KiCad to design the keyboard. Here is a capture of the board (the KiCad
project files are right here, in the root of this repository):

![PCB](pcb.png)*The common PCB, from which ANSI and ISO are extracted*

Since the idea is to have the most compact possible keyboard, the board occupies
only what the keys occupy. This forced me to put the microcontroller below the
space bar, and put a looooong track for the USB connector. This should be no problem
since my idea was to use Low-Speed USB, which works at 1.5Mbps. At those speeds,
impedance coupling is not a problem. But since I wanted to ensure that it would
also work at Full-Speed USB (12Mbps), I decided to use a matched differential
pair, so the length of the USB connection should be no problem.

The connector is a micro-USB. I could have used an USB-C one just by adding two
resistors, but I preferred to stick to what I know.

Finally, the microcontroller is an AtMega32U4, just like the one in the 8-bit Teensy
boards. This allows to reuse the currently available software. It can be programmed
using the J5 connector (although I recommend to not mount a connector there, but
just weld the cables temporarily, program the microcontroller, and remove them
after doing that, using software programming for any update).

Although working at Low-Speed doesn't need a quartz crystal, I strongly recommend
using a 16MHz one, like the Teensy 2.0 board. According to the microcontroller
documentation, an external clock is only required to work at USB Full-Speed, while
for Low-Speed, the internal, factory-calibrated RC oscillator has enough precision.
So, in theory, you can avoid mounting Y0, C5 and C6. But it is important to take
into account that there are two kind of Atmega32u4: the "normal" one, and the
"RC" one. The former comes pre-configured to use an external clock, while the
later comes pre-configured to use the internal RC. Also, the SPI programming
interface requires an active clock, which means that if you buy a "normal"
version, you must connect a quartz. Of course, you can then change the configuration
fuses to use the internal RC and only then you can remove the quartz.

Also take into account that the factory RC calibration is for 3 volts, so you
will have to re-calibrate the RC oscillator for this board if you decide to go
crystal-less , since it works at 5 volts.

Finally, when you create some software, remember that you must enable the USB
power regulator. In general, you need the same configuration than a Teensy 2.0
board.

I also added three NeoPixels, connected to the SPI's MOSI pin to simplify the
control. Since this pin is also used for programming the uC, it is possible that
during the operation, the NeoPixels will change their color randomly. To avoid
this, there is the R3 resistor, which has a value of 0ohm (this is, it's just
a bridge). Removing it will disable the NeoPixels, so the recommendation is to
first program the uC and only then mount the resistor.

To make the board more flexible, it is possible to mount an SMD NeoPixel, either
the 5x5mm "normal version" or the 3.5x3.5 "mini version".

Finally, there is a RESET button added just in case. Since it should be no need
to press it regularly, it should be enough to leave a little hole where to insert
a needle or a pin to press it. It is usual to have a bootloader that switches to
"programming mode" when the button is pressed.

![3D design](3dview.png)*3D rendering of the ISO layout*

The current design uses 45, 90 and 135 degree angles, but the board is specifically
designed to allow to apply the "Round tracks" KiCad extension. For that, apply it
only to the *Default* and *Power* net types, but not to the *usb bus* net type,
because that must be kept *as-is*.

![Rounded PCB](pcb2.png)*PCB with rounded tracks*

## 3D printed case

The folder *3dcase* contains the FreeCAD design for a case that can be 3D
printed at home.

![3D case design](3dcase_design.png)*FreeCAD with all the pieces for the case*

Also inside that folder is a *README.md* file with the instructions to
build it. Although ideally you should use a 3D printer with a big bed, it can
also be printed in any printer with a surface of, at least, 13x17 cm.

## The firmware

The folder *firmware* contains, as expected, the firmware code.

Current firmware is written in C language using Arduino/teensyduino libraries.
I also use *ubaboot* (<https://github.com/rrevans/ubaboot>) for convenience, but
it isn't mandatory.

It is based on an event queue, and implements asynchronous functions in C. This
allows to create concurrent tasks in an easy way.

The current firmware (as of version 1.13) is fast enough as to read the keyboard
in about 400us. This allows it to work at 1KHz polling rate, since the limit is in
the USB library. Also, at the moment of writing this, the firmware has only the
basic functionality, but in the future I want to add extras, like use the *Fn*
key for macros, and export the NeoPixeles through USB.

The firmware has support for macros; just edit the *macro_list* array in the
macros.cpp file, adding those that you want. The macros are executed by pressing
the specified key plus the *Fn* key. If a key has no macro assigned, the keyboard
will send the specified key plus *left shift*+*left control*+*left windows*+
*left alt*. This allows to use these combinations for specific macros in programs.

## The result

Here are two keyboards fully built, with the 3D printed case. Notice that they
are V1.0 versions because they have a single NeoPixel.

![Built keyboards](built.jpg)*Two keyboards fully built*

## Alternative control

It is possible to not mount the microcontroller and USB connector, and connect the
keyboard directly using the two 10-pad zones where all the rows and columns are
directly available. This can be used, for example, for connecting it directly to
the I/O pins of a Raspberry PI.

## Geometry

The keyboard is divided in 8 columns and 12 semi-rows, thus reducing the number of
I/O pins required to the bare minimum. Each row is divided in two blocks, thus the
column 0 and the 15 are the same, the 1 and the 14, and so on.

![Keyboard matrix](matrix.png "The keyboard matrix")

## Versions

* v1.13
  * Fixed a track that made an ugly result with the "round-tracks" module
  * Added the name (MiniTKL) in the schematics
  * Updated the firmware
* v1.12
  * Added a third NeoPixel
  * Better precision in the firmware for bounce suppression
  * Added case design (ready to be 3D printed) and instructions to build it
* v1.11
  * Better holes for the dual ISO/ANSI support for the left shift key
  * Added a second NeoPixel
  * Removed NeoPixel Diffusor support
* v1.10
  * Changed the order of the navigation keys, moving HOME and END down
  * Added an ANSI layout picture in the README
* v1.9
  * Moved a diode to leave more space for a support in the (future) case
* v1.8
  * Fixed width of one VCC track (it was 0.3, now it is 0.5, as all the others)
  * Added the quartz value to the schematic (16MHz)
  * Now both ISO and ANSI layouts support short and long Left Shift Key
* v1.7
  * Added labels to the matrix pads
  * Written the ports used in each row and column in the matrix picture
  * Added bootloader resistor in PE2, moved column 6 from PE2 to PB7, and
      removed C7. All to improve Teensy 2.0 compatibility.
  * Added revision number in the board and the schematics
* v1.6
  * Added dual pads to support both normal and mini NeoPixels (and also
      NeoPixel Diffusor)
  * Some track re-routing to simplify even further the creation of the ANSI and
      ISO versions
* v1.5
  * Adjusted again the length of the USB differential pair
* v1.4
  * Some changes to simplify creating the ANSI and ISO versions
  * Added a 0ohm resistor in the NeoPixel data line
  * Changed the recommendation about using a quartz (it IS very recommended)
  * Added two white zones to allow to write things using a permanent marker
  * Added labels to all the PADs
* v1.3
  * Some updates to avoid warnings when using rounded tracks
  * Some adjustments in the VTS tracks
* v1.2
  * Added Transient Voltage Suppression chip
  * Moved some elements from place
  * Updated the documentation
* v1.1:
  * Converted to dual layout (allowing ISO and ANSI layouts)
  * Centered the NeoPixel
  * Changed the programming connector to the Atmega standard
* v1.0:
  * First version (ISO only)

## Author

Sergio Costas (Raster Software Vigo)  
rastersoft@gmail.com  
<https://www.rastersoft.com>
